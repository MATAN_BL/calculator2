window.onload = function() {
    'use strict';
    let operation = "add", prevNum = 0, prevDecPnt = 0, curNum = 0, curNumStr = "0", curDecPnt = 0, isCurFraction = false;

    const getNewOperation = function getNewOperation(id) {
        switch (id) {
            case "add":
                operation = "add";
                break;
            case "sub":
                operation = "sub";
                break;
            case "mul":
                operation = "mul";
                break;
            case "div":
                operation = "div";
                break;
            case "eq":
                operation = "none";
                break;
        }
    };

    const makeOperation = function makeOperation() {
        switch(operation) {
            case "add":
                prevNum = prevNum + curNum;
                prevDecPnt = Math.max(curDecPnt, prevDecPnt);
                break;
            case "sub":
                prevNum = prevNum - curNum;
                prevDecPnt = Math.max(curDecPnt, prevDecPnt);
                break;
            case "mul":
                prevNum = prevNum * curNum;
                prevDecPnt = curDecPnt + prevDecPnt;
                break;
            case "div":
                prevNum = prevNum / curNum;
                prevDecPnt = getNumOfDigitsRightToPnt(prevNum);
                break;
            case "none":
                prevNum = curNum;
                prevDecPnt = curDecPnt;
                break;
        }
        showPrevNum();
    };

    const showPrevNum = function showPrevNum() {
        let prevNumStr;
        if (prevDecPnt === 0) {
            prevNumStr = prevNum.toString();
        } else {
            prevNumStr = prevNum.toFixed(prevDecPnt);
        }
        document.getElementById('input').value = removeRedunZeros(prevNumStr, prevDecPnt);
    };

    const removeRedunZeros = function removeRedunZeros(num, numOfZeros) {
        if (num.indexOf('.') === -1) {
            return num;
        }
        let strSplit = num.split('.');
        let i = strSplit[1].length - 1;
        while (i >= 0) {
            if (strSplit[1][i] === '0') {
                i--;
            } else {
                break;
            }
        }
        if (i === -1) {
            return strSplit[0]
        } else {
            return strSplit[0] + '.' + strSplit[1].substring(0, i + 1);
        }
    };

    const getNumOfDigitsRightToPnt = function getNumOfDigitsRightToPnt(num) {
        let strNum = num.toString();
        if (strNum.indexOf('.') === -1) {
            return 0;
        } else {
            return strNum.split('.')[1].length;
        }
    };

    const resetValues = function resetValues() {
        operation = "add";
        prevNum = 0;
        prevDecPnt = 0;
        resetCurNum();
    };

    const resetCurNum = function resetCurNum() {
        isCurFraction = false;
        curNum = 0;
        curNumStr = "0";
        curDecPnt = 0;
    };

    (function() {
        let numButtonArray = Array.from(document.getElementsByClassName('num-btn'));
        numButtonArray.forEach((btn) => {
            btn.addEventListener("click", (event) => {
                if (isCurFraction) {
                   curDecPnt++;
                   curNum = curNum + Math.pow(10, (-1)*curDecPnt) * parseInt(btn.innerText);
                   if (curNum.toString().indexOf('.') === -1) {
                       curNumStr = curNumStr + "0";
                   } else {
                       curNumStr = curNum.toFixed(curDecPnt);
                   }
                } else {
                   curNum = curNum * 10 + parseInt(event.target.innerText);
                   curNumStr = curNum.toString();
                }
                document.getElementById('input').value = curNumStr;
            });
        });

        document.getElementById('dec-btn').addEventListener("click", () => {
            if (curDecPnt === 0) {
                isCurFraction = true;
                curNumStr = curNum + '.';
            }
        });

        document.getElementById('c-btn').addEventListener("click", () => {
            resetValues();
            document.getElementById('input').value = "0";
        });

        let opButtonArray = Array.from(document.getElementsByClassName('op-btn'));
        opButtonArray.forEach((btn) => {
            btn.addEventListener("click", (event) => {
                if (operation !== "none") {
                    makeOperation();
                }
                resetCurNum();
                getNewOperation(event.target.id);
            })
        });
    })();
};

